document.write(`<header><p> Домашнее задание №1</p></header>`)
        function Human(name, surname, age){
            this.name1 = name;
            this.surname1 = surname;
            this.age1 = age;
            this.say = function(){
                document.write(`<div class="div"><p class="p"> Информация о человеке: ${this.name1} ${this.surname1}, ${this.age1}; </p></div>`) 
            }
        }

        const arr = [
            new Human("Иван", "Дуб", 43),
            new Human("Александр", "Вареник", 23),
            new Human("Руслан", "Кужельнов", 19), 
            new Human("Роман", "Филонюк", 30)
        ];
        
        arr.sort( (a, b) => (a.age1 > b.age1) ? 1 : -1 );
        
        arr[0].say();
        arr[1].say();
        arr[2].say();
        arr[3].say();

        document.write(`<header><p> Домашнее задание №2</p></header>`)

        function Human1(name, surname, age, b){
            this.name1 = name;
            this.surname1 = surname;
            this.age = prompt("Введите свой возраст: "), this.age = parseInt(this.age);
            this.b = prompt("Введите число от 1 до 5(числа вереводятся в года): "), this.b = parseInt(this.b);
            this.say = function(){
                document.write(`<div><p> Информация о Директоре: ${this.name1} ${this.surname1}, ${this.age}; </p></div>`) 
            }
        }

        Human1.prototype.getFunction = function(){
            if(this.b == "1")
            {
                return `Ваш возраст через 1 год: ${this.age + 1}`;
            }
            else if(this.b == "2")
            {
                return `Ваш возраст через 2 года: ${this.age + 2}`;
            }
            else if(this.b == "3")
            {
                return `Ваш возраст через 3 года: ${this.age + 3}`;
            }
            else if(this.b == "4")
            {
                return `Ваш возраст через 4 года: ${this.age + 4}`;
            }
            else(this.b == "5")
            {
                return `Ваш возраст через 5 лет: ${this.age + 5}`;
            }
        }
        const h = new Human1("Руслан", "Кужельнов", 19, "Года");
        h.say();
        document.write("<div><p>" + h.getFunction() + "</p></div>");